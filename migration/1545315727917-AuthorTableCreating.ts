import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class ClientTableAuthor1545315727917 implements MigrationInterface {

  // @ts-ignore
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'author',
      columns: [{
        name: 'id',
        type: 'int',
        isPrimary: true,
        isGenerated: true,
        generationStrategy: 'increment',
      }, {
        name: 'name',
        type: 'varchar',
        charset: 'utf8',
      }, {
        name: 'surname',
        type: 'varchar',
        charset: 'utf8',
      }, {
        name: 'birthday',
        type: 'int',
        default: null,
        isNullable: true,
      }],
    }), true);

    await queryRunner.createIndex('author', new TableIndex({
      name: 'IDX_AUTHOR_NAME',
      columnNames: ['name'],
    }));

    await queryRunner.createIndex('author', new TableIndex({
      name: 'IDX_AUTHOR_SURNAME',
      columnNames: ['surname'],
    }));
  }

  // @ts-ignore
  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('author', true);
  }

}
