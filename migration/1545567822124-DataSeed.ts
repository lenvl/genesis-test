import { MigrationInterface, QueryRunner } from 'typeorm';

export class DataSeed1545567822124 implements MigrationInterface {

  // @ts-ignore
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (1,\'Уильям\', \'Шекспир\', 234242)');
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (2,\'Уильям\', \'Фолкнер\', 3453456)');
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (3,\'Антон\', \'Чехов\', 234242)');
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (4,\'Джоан\', \'Роулинг\', 45356)');
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (5,\'Лев\', \'Толстой\', 234242)');
    await queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (6,\'Валерий\', \'Милько\', 14323)');

    await queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (1,\'Сергей\', \'Яковенко\')');
    await queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (2,\'Катя\', \'Иванова\')');
    await queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (3,\'Владислав\', \'Лень\')');
    await queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (4,\'Андрей\', \'Капустин\')');
    //
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (1, 1, \'Обесчещенная Лукреция\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (2,2, \'Роза для Эмили\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (3,3, \'Чайка\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (4,4, \'Гарри Поттер и Проклятое дитя\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (5,5, \'Война и Мир\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (6,6, \'Жизнь воровская\')');
    await queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (7,6, \'Жизнь воровская том 2\')');

    await queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (1,6, 3)');
    await queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (2,7, 3)');
    await queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (3,4, 2)');
  }

  // @ts-ignore
  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
