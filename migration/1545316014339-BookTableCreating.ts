import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class BookTableCreating1545316014339 implements MigrationInterface {

  // @ts-ignore
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'book',
      columns: [{
        name: 'id',
        type: 'int',
        isPrimary: true,
        isGenerated: true,
        generationStrategy: 'increment',
      }, {
        name: 'pub_date',
        type: 'TIMESTAMP',
        default: 'CURRENT_TIMESTAMP',
      }, {
        name: 'name',
        charset: 'utf8',
        type: 'varchar',
        isNullable: false,
      }, {
        name: 'author_id',
        type: 'int',
        isNullable: true,
      }],
    }), true);

    await queryRunner.createIndex('book', new TableIndex({
      name: 'IDX_BOOK_NAME',
      columnNames: ['name'],
    }));

    await queryRunner.createForeignKey('book', new TableForeignKey({
      name: 'FK_BOOK_AUTHOR_ID',
      columnNames: ['author_id'],
      onDelete: 'CASCADE',
      referencedTableName: 'author',
      referencedColumnNames: ['id'],
    }));
  }

  // @ts-ignore
  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('book');
  }

}
