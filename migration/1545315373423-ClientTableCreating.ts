import { MigrationInterface, QueryRunner, Table, TableColumn, TableIndex } from 'typeorm';

export class ClientTableCreating1545315373423 implements MigrationInterface {

  // @ts-ignore
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'client',
      columns: [
        new TableColumn({
          name: 'id',
          type: 'integer',
          isPrimary: true,
          isGenerated: true,
          generationStrategy: 'increment',
        }),
        new TableColumn({
          name: 'surname',
          type: 'varchar',
          charset: 'utf8',
          isPrimary: false,
        }),
        new TableColumn({
          name: 'name',
          type: 'varchar',
          charset: 'utf8',
          isPrimary: false,
        }),
        new TableColumn({
          name: 'created_at',
          type: 'TIMESTAMP',
          default: 'CURRENT_TIMESTAMP',
        }),
      ],
    }));

    await queryRunner.createIndex('client', new TableIndex({
      name: 'IDX_CLIENT_NAME',
      columnNames: ['name'],
    }));

    await queryRunner.createIndex('client', new TableIndex({
      name: 'IDX_CLIENT_SURNAME',
      columnNames: ['surname'],
    }));
  }

  // @ts-ignore
  public async down(queryRunner: QueryRunner): Promise<any> {
    return queryRunner.dropTable('client', true);
  }

}
