import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm';

export class LockedBookTableCreating1545316477837 implements MigrationInterface {

  // @ts-ignore
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'locked_book',
      columns: [{
        name: 'id',
        type: 'int',
        isPrimary: true,
        isGenerated: true,
        generationStrategy: 'increment',
      }, {
        name: 'book_id',
        type: 'int',
      }, {
        name: 'client_id',
        type: 'int',
      }],
    }), true);

    await queryRunner.createForeignKey('locked_book', new TableForeignKey({
      name: 'FK_LOCKED_BOOK_BOOK_ID',
      columnNames: ['book_id'],
      onDelete: 'CASCADE',
      referencedColumnNames: ['id'],
      referencedTableName: 'book',
    }));

    await queryRunner.createForeignKey('locked_book', new TableForeignKey({
      name: 'FK_LOCKED_BOOK_CLIENT_ID',
      columnNames: ['client_id'],
      onDelete: 'CASCADE',
      referencedColumnNames: ['id'],
      referencedTableName: 'client',
    }));
  }

  // @ts-ignore
  public async down(queryRunner: QueryRunner): Promise<any> {
    return queryRunner.dropTable('locked_book');
  }

}
