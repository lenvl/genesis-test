"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var DataSeed1545567822124 = /** @class */ (function () {
    function DataSeed1545567822124() {
    }
    // @ts-ignore
    DataSeed1545567822124.prototype.up = function (queryRunner) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (1,\'Уильям\', \'Шекспир\', 234242)')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (2,\'Уильям\', \'Фолкнер\', 3453456)')];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (3,\'Антон\', \'Чехов\', 234242)')];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (4,\'Джоан\', \'Роулинг\', 45356)')];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (5,\'Лев\', \'Толстой\', 234242)')];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `author` (`id`, `name`, `surname`, `birthday`) VALUES (6,\'Валерий\', \'Милько\', 14323)')];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (1,\'Сергей\', \'Яковенко\')')];
                    case 7:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (2,\'Катя\', \'Иванова\')')];
                    case 8:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (3,\'Владислав\', \'Лень\')')];
                    case 9:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `client` (`id`, `name`, `surname`) VALUES (4,\'Андрей\', \'Капустин\')')];
                    case 10:
                        _a.sent();
                        //
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (1, 1, \'Обесчещенная Лукреция\')')];
                    case 11:
                        //
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (2,2, \'Роза для Эмили\')')];
                    case 12:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (3,3, \'Чайка\')')];
                    case 13:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (4,4, \'Гарри Поттер и Проклятое дитя\')')];
                    case 14:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (5,5, \'Война и Мир\')')];
                    case 15:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (6,6, \'Жизнь воровская\')')];
                    case 16:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `book` (`id`, `author_id`, `name`) VALUES (7,6, \'Жизнь воровская том 2\')')];
                    case 17:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (1,6, 3)')];
                    case 18:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (2,7, 3)')];
                    case 19:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query('INSERT INTO `locked_book` (`id`,`book_id`, `client_id`) VALUES (3,4, 2)')];
                    case 20:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // @ts-ignore
    DataSeed1545567822124.prototype.down = function (queryRunner) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    return DataSeed1545567822124;
}());
exports.DataSeed1545567822124 = DataSeed1545567822124;
