FROM node:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

ENV DB_HOST=8.9.31.216
ENV DB_USER=root
ENV DB_NAME=test_db
ENV DB_PASSWORD=vfhbyjxrf123

EXPOSE 3000
CMD ["tsc", "./migration/*.ts"]
CMD ["typeorm", " migration:run"]
CMD ["npm", "run", "start:prod"]
