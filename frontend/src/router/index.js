import Vue from 'vue';
import Router from 'vue-router';
import Clients from '@/components/Index';
import ClientPage from '@/components/Client';
import AdminPage from '@/components/admin/Index';
import AuthorPage from '@/components/admin/subpages/SingleAuthorPage';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Clients',
      component: Clients,
    },
    {
      path: '/client/:id',
      name: 'ClientPage',
      component: ClientPage,
    },
    {
      path: '/admin',
      name: 'AdminPage',
      component: AdminPage,
    },
    {
      path: '/admin/:page',
      name: 'AdminPage',
      component: AdminPage,
    },
    {
      path: '/admin/author/:author',
      name: 'AuthorPage',
      component: AuthorPage,
    },
  ],
});
