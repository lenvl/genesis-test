import request from '../utils/request';

export default class AdminAPI {
  static async getAllAuthors() {
    const result = await request.get('/api/admin/author/list');
    return result.data;
  }
  static async createAuthor(payload) {
    const result = await request.post('/api/admin/author/create', payload);
    return result.data;
  }
  static async createBook(payload) {
    const result = await request.post('/api/admin/book/create', payload);
    return result.data;
  }
  static async getAuthorById(authorId) {
    const result = await request.get(`/api/admin/author/${authorId}`);
    return result.data;
  }
  static async getBooksByAuthor(authorId) {
    const result = await request.get(`/api/admin/author/books/${authorId}`);
    return result.data;
  }
  static async removeAuthor(authorId) {
    const result = await request.delete(`/api/admin/author/${authorId}`);
    return result.data;
  }
  static async removeBook(bookId) {
    const result = await request.delete(`/api/admin/book/${bookId}`);
    return result.data;
  }
}
