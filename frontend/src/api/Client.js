import request from '../utils/request';

export default class ClientAPI {
  static async getAllClients() {
    const result = await request.get('/api/client/list');
    return result.data;
  }

  static async getClientInfo(clientId) {
    const result = await request.get(`/api/client/${clientId}`);
    return result.data;
  }
}
