import request from '../utils/request';

export default class BookAPI {
  static async getBooksForClient(clientId) {
    const result = await request.get(`/api/book/locked-by-client/${clientId}`);
    return result.data;
  }
  static async getAvailableBooksFor() {
    const result = await request.get('/api/book/available');
    return result.data;
  }
  static async changeLockState(clientId, islocked, bookId) {
    let action = 'lock';
    if (islocked) action = 'un-lock';
    const result = await request.post(`/api/book/${action}`, {
      client_id: clientId,
      book_id: bookId,
    });
    return result.data;
  }
}
