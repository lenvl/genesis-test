import { InjectRepository } from '@nestjs/typeorm';
import { AuthorEntity } from './entities/author-entity.dto';
import { DeleteResult, Repository } from 'typeorm';
import { isNumber } from 'util';
import { NotFoundException, Injectable } from '@nestjs/common';
import { InvalidArgsException } from '../../exceptions/invalid-args.exception';

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(AuthorEntity)
    private readonly authorRepository: Repository<AuthorEntity>,
  ) {}

  public async getAllAuthors(): Promise<AuthorEntity[]> {
    return this.authorRepository.createQueryBuilder('author').getMany();
  }

  public async createAuthor(name: string, surname: string, birthday: number): Promise<AuthorEntity> {
    const author = new AuthorEntity();
    author.name = name;
    author.surname = surname;
    author.birthday = birthday;
    return this.authorRepository.save(author);
  }

  public async deleteauthorById(authorId: number): Promise<AuthorEntity> {
    const author = await this.getAuthorById(authorId);
    return this.authorRepository.remove(author);
  }

  /**
   * @throws
   * @param author_id
   */
  public async getAuthorById(author_id: number): Promise<AuthorEntity> {
    if (!author_id) throw new InvalidArgsException();
    return this.authorRepository.createQueryBuilder('author')
      .leftJoinAndSelect('author.books', 'books')
      .where('author.id = :author_id', {
        author_id,
      }).getOne();
  }

  /**
   * @throws
   * @param author_id
   */
  public async removeAuthorById(author_id: number): Promise<AuthorEntity> {
    if (!isNumber(author_id)) throw new InvalidArgsException();
    const author = await this.getAuthorById(author_id);
    if (!author) throw new NotFoundException('author_with_target_id_not_found');
    return this.authorRepository.remove(author);
  }
}
