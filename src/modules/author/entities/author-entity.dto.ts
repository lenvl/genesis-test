import { Entity, PrimaryGeneratedColumn, Column, Index, OneToMany } from 'typeorm';
import { BookEntity } from '../../book/entities/book.entity';

@Entity({
  name: 'author',
})
export class AuthorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @Index()
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @Index()
  surname: string;

  @Column({
    type: 'timestamp',
  })
  birthday: number;

  @OneToMany(() => BookEntity, book => book.author)
  books: BookEntity[];
}
