import { Controller, Get, Param } from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorEntity } from './entities/author-entity.dto';

@Controller('/api/author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @Get('/list')
  public async getAuthorList(): Promise<AuthorEntity[]> {
    return this.authorService.getAllAuthors();
  }

  @Get('/get-one/:author_id')
  public async getAuthorById(@Param() param): Promise<AuthorEntity> {
    return this.authorService.getAuthorById(param.author_id);
  }

}
