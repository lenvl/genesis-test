import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorEntity } from './entities/author-entity.dto';
import { AuthorService } from './author.service';
import { AuthorController } from './author.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([AuthorEntity]),
  ],
  providers: [AuthorService],
  exports: [AuthorService],
  controllers: [AuthorController],
})
export class AuthorModule {}
