import { Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { BookEntity } from '../../book/entities/book.entity';
import { ClientEntity } from '../../client/entities/client.entity';

@Entity({
  name: 'locked_book',
})
export class LockedBook {
  @PrimaryGeneratedColumn()
  id: number;

  @JoinColumn({
    name: 'client_id',
  })
  @ManyToOne(() => ClientEntity, client => client.locked_books)
  client: ClientEntity;

  @JoinColumn({
    name: 'book_id',
  })
  @OneToOne(() => BookEntity, book => book.lock)
  book: BookEntity;
}
