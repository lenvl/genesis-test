import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LockedBook } from './entities/locked-book.entity';
import { ClientModule } from '../client/client.module';
import { LockedBookService } from './locked-book.service';
import { ClientService } from '../client/client.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([LockedBook]),
    ClientModule,
  ],
  providers: [LockedBookService, ClientService],
  exports: [LockedBookService],
})
export class LockedBookModule {}
