import { InjectRepository } from '@nestjs/typeorm';
import { LockedBook } from './entities/locked-book.entity';
import { Repository, DeleteResult } from 'typeorm';
import { ClientEntity } from '../client/entities/client.entity';
import { ClientService } from '../client/client.service';
import { isNumber } from 'util';
import { InvalidArgsException } from '../../exceptions/invalid-args.exception';
import { OutOfLockRangeException } from '../../exceptions/out-of-lock-range.exception';
import { BookEntity } from '../book/entities/book.entity';

export class LockedBookService {
  private maxBookForClient: number = 5;

  constructor(
    @InjectRepository(LockedBook)
    public readonly lockedBookRepository: Repository<LockedBook>,
    private readonly clientService: ClientService,
  ) {
  }

  public async getLockedBooksForClient(client_id: number): Promise<LockedBook[]> {
    return this.lockedBookRepository.createQueryBuilder('locked')
      .leftJoinAndSelect('locked.book', 'book')
      .leftJoinAndSelect('book.author', 'author')
      .leftJoinAndSelect('locked.client', 'client')
      .where('locked.client_id = :client_id', {
        client_id,
      }).getMany();
  }

  public async lockBook(book: BookEntity, client_id: number): Promise<LockedBook> {
    if (!isNumber(client_id)) throw new InvalidArgsException();
    const client = await this.clientService.findClientById(client_id);
    const alreadyLocked = await this.getLockedBooksForClient(client_id);
    if (alreadyLocked.length >= this.maxBookForClient) throw new OutOfLockRangeException();
    const lockRecord = new LockedBook();
    lockRecord.book = book;
    lockRecord.client = client;
    return this.lockedBookRepository.save(lockRecord);
  }

  public async unlockBook(book: BookEntity, client_id: number): Promise<DeleteResult> {
    if (!client_id) throw new InvalidArgsException();
    const client = await this.clientService.findClientById(client_id);
    return this.lockedBookRepository.delete({
      client,
      book,
    });
  }
}
