import { Module } from '@nestjs/common';
import { AuthorModule } from '../author/author.module';
import { BookModule } from '../book/book.module';
import { AuthorService } from '../author/author.service';
import { BookService } from '../book/book.service';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';

@Module({
  imports: [AuthorModule, BookModule],
  providers: [AuthorService, BookService, AdminService],
  controllers: [AdminController],
})
export class AdminModule {}
