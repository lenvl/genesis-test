import { Injectable } from '@nestjs/common';
import { BookService } from '../book/book.service';
import { AuthorService } from '../author/author.service';
import { AuthorEntity } from '../author/entities/author-entity.dto';
import { CreateAuthorDto } from './dtos/create-author.dto';
import { CreateBookDto } from './dtos/create-book.dto';
import { BookEntity } from '../book/entities/book.entity';

@Injectable()
export class AdminService {
  constructor(
    private readonly bookService: BookService,
    private readonly authorService: AuthorService,
  ) {
  }

  public async deleteAuthorById(authorId: number): Promise<AuthorEntity> {
    return this.authorService.deleteauthorById(authorId);
  }

  public async createAuthor(payload: CreateAuthorDto): Promise<AuthorEntity> {
    return this.authorService.createAuthor(payload.name, payload.surname, payload.birthday);
  }

  public async getBooksByAuthor(authorId: number): Promise<BookEntity[]> {
    return this.bookService.getBooksByAuthor(authorId);
  }

  public async getAuthors(): Promise<AuthorEntity[]> {
    return this.authorService.getAllAuthors();
  }

  public async getAuthorById(authorId: number): Promise<AuthorEntity> {
    return this.authorService.getAuthorById(authorId);
  }

  public async removeBookById(bookId: number): Promise<boolean> {
    await this.bookService.removeBookById(bookId);
    return true;
  }

  public async createBook(payload: CreateBookDto): Promise<BookEntity> {
    return this.bookService.createBook(payload.author_id, payload.name);
  }
}
