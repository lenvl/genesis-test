import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { AdminService } from './admin.service';
import { CreateAuthorDto } from './dtos/create-author.dto';
import { CreateBookDto } from './dtos/create-book.dto';

@Controller('/api/admin')
export class AdminController {
  constructor(
    private readonly adminService: AdminService,
  ) {
  }

  @Get('/author/list')
  public async getAuthors() {
    return this.adminService.getAuthors();
  }

  @Get('/author/:author_id')
  public async getAuthor(@Param() param) {
    return this.adminService.getAuthorById(param.author_id);
  }

  @Delete('/author/:author_id')
  public async deleteAuthor(@Param() param) {
    return this.adminService.deleteAuthorById(param.author_id);
  }

  @Post('/author/create')
  public async createAuthor(@Body() payload: CreateAuthorDto) {
    return this.adminService.createAuthor(payload);
  }

  @Get('/author/books/:author_id')
  public async getAuthorBooks(@Param() param) {
    return this.adminService.getBooksByAuthor(param.author_id);
  }

  @Delete('/book/:book_id')
  public async deleteBookById(@Param() params) {
    return this.adminService.removeBookById(params.book_id);
  }

  @Post('/book/create')
  public async createBook(@Body() payload: CreateBookDto) {
    return this.adminService.createBook(payload);
  }
}
