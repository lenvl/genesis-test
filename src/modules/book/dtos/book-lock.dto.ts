import { IsNotEmpty, IsNumber } from 'class-validator';

export class BookLockDTO {
  @IsNotEmpty()
  @IsNumber()
  book_id: number;

  @IsNotEmpty()
  @IsNumber()
  client_id: number;
}
