import { Entity, PrimaryGeneratedColumn, JoinColumn, Column, ManyToOne, OneToMany, OneToOne } from 'typeorm';
import { AuthorEntity } from '../../author/entities/author-entity.dto';
import { LockedBook } from '../../lockedbook/entities/locked-book.entity';

@Entity({
  name: 'book',
})
export class BookEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
  })
  pub_date: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @JoinColumn({
    name: 'author_id',
  })
  @ManyToOne(() => AuthorEntity, author => author.books)
  author: AuthorEntity;

  @OneToOne(() => LockedBook, lock => lock.book)
  lock: LockedBook;
}
