import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookEntity } from './entities/book.entity';
import { AuthorModule } from '../author/author.module';
import { BookService } from './book.service';
import { AuthorService } from '../author/author.service';
import { BookController } from './book.controller';
import { LockedBookModule } from '../lockedbook/locked-book.module';
import { LockedBookService } from '../lockedbook/locked-book.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([BookEntity]),
    AuthorModule,
    LockedBookModule,
  ],
  providers: [BookService, AuthorService, LockedBookService],
  exports: [BookService],
  controllers: [BookController],
})
export class BookModule {}
