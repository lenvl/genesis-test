import { BookService } from './book.service';
import { Controller, Get, Post, Body, HttpException, HttpStatus, Param } from '@nestjs/common';
import { BookLockDTO } from './dtos/book-lock.dto';
import { LockedBookService } from '../lockedbook/locked-book.service';
import { BookEntity } from './entities/book.entity';
import { LockedBook } from '../lockedbook/entities/locked-book.entity';
import { DeleteResult } from 'typeorm';

/**
 * Have some problem with circular dependencies.
 * I dont have a lot of time to think about graceful decision
 * So, in routes /lock, /un-lock I'll use LockedBookService in BookController :(
 */

@Controller('/api/book')
export class BookController {
  constructor(
    private readonly bookService: BookService,
    private readonly lockedBookService: LockedBookService,
  ) {
  }

  @Get('/available')
  public async getAvailableBooks(): Promise<BookEntity[]> {
    return this.bookService.getAllAvailableBooks();
  }

  @Post('/lock')
  public async lockBook(@Body() payload: BookLockDTO): Promise<LockedBook> {
    const book = await this.bookService.getBookById(payload.book_id);
    if (!book) throw new HttpException('book_not_found', HttpStatus.NOT_FOUND);
    return this.lockedBookService.lockBook(book, payload.client_id);
  }

  @Post('/un-lock')
  public async unlockBook(@Body() payload: BookLockDTO): Promise<DeleteResult> {
    const book = await this.bookService.getBookById(payload.book_id);
    if (!book) throw new HttpException('book_not_found', HttpStatus.NOT_FOUND);
    return this.lockedBookService.unlockBook(book, payload.client_id);
  }

  @Get('/locked-by-client/:client_id')
  public async getClientBooks(@Param() param) {
    return this.lockedBookService.getLockedBooksForClient(param.client_id);
  }
}
