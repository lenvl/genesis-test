import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity } from './entities/book.entity';
import { Repository, DeleteResult } from 'typeorm';
import { AuthorService } from '../author/author.service';
import { isNumber } from 'util';
import { InvalidArgsException } from '../../exceptions/invalid-args.exception';
import { LockedBookService } from '../lockedbook/locked-book.service';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(BookEntity)
    private readonly bookRepository: Repository<BookEntity>,
    private readonly authorService: AuthorService,
    private readonly lockedBookService: LockedBookService,
  ) {
  }

  public async createBook(author_id: number, book_name: string): Promise<BookEntity> {
    const author = await this.authorService.getAuthorById(author_id);
    const book = new BookEntity();
    book.author = author;
    book.name = book_name;
    return this.bookRepository.save(book);
  }

  public async getBookById(book_id: number): Promise<BookEntity> {
    console.log(book_id);
    if (!book_id) throw new InvalidArgsException();
    return this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.author', 'author')
      .where('book.id = :id', { id: book_id })
      .getOne();
  }

  public async removeBookById(book_id: number): Promise<DeleteResult> {
    if (!book_id) throw new InvalidArgsException();
    return this.bookRepository.delete({
      id: book_id,
    });
  }

  public async getAllAvailableBooks() {
    const lockedBooksSubQb = this.lockedBookService.lockedBookRepository
      .createQueryBuilder('lb')
      .select('book_id');
    const qb = this.bookRepository.createQueryBuilder('book');
    qb.where(`book.id NOT IN (${lockedBooksSubQb.getQuery()})`);
    qb.leftJoinAndSelect('book.author', 'author');
    return qb.getMany();
  }

  public async getAllBooks() {
    return this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.author', 'author')
      .getMany();
  }

  public async getBooksByAuthor(authorId) {
    return this.bookRepository
      .createQueryBuilder('book')
      .where('book.author_id = :author_id', { author_id: authorId })
      .getMany();
  }
}
