import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClientEntity } from './entities/client.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(ClientEntity)
    private readonly clientRepository: Repository<ClientEntity>,
  ) {
  }

  public async getAllClients(): Promise<ClientEntity[]> {
    return this.clientRepository.find();
  }

  public async findClientById(id: number): Promise<ClientEntity> {
    return this.clientRepository.findOne({
      id,
    });
  }

}
