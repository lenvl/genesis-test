import { Controller, Get, Param, HttpException, HttpStatus } from '@nestjs/common';
import { ClientService } from './client.service';
import { ClientEntity } from './entities/client.entity';

@Controller('/api/client')
export class ClientController {

  constructor(
    private readonly clientService: ClientService,
  ) {}

  @Get('/list')
  public async getAllClient(): Promise<ClientEntity[]> {
    return this.clientService.getAllClients();
  }

  @Get('/:client_id')
  public async getClient(@Param() param): Promise<ClientEntity> {
    const client = await this.clientService.findClientById(param.client_id);
    if (!client) throw new HttpException('err_client_not_found', HttpStatus.BAD_REQUEST);
    return client;
  }
}
