import { Entity, PrimaryGeneratedColumn, Column, Index, OneToMany } from 'typeorm';
import { LockedBook } from '../../lockedbook/entities/locked-book.entity';

@Entity({
  name: 'client',
})
export class ClientEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @Index()
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @Index()
  surname: string;

  @Column({
    type: 'timestamp',
    default: 'CURRENT_TIMESTAMP',
  })
  created_at: number;

  @OneToMany(() => LockedBook, lock => lock.book)
  locked_books: LockedBook[];
}
