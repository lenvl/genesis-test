import { HttpException, HttpStatus } from '@nestjs/common';

export class InvalidArgsException extends HttpException {
  constructor() {
    super('invalid_args_exception', HttpStatus.BAD_REQUEST);
  }
}
