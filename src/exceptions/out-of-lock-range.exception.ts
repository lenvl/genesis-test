import { HttpException, HttpStatus } from '@nestjs/common';

export class OutOfLockRangeException extends HttpException {
  constructor() {
    super('max_count_of_locked_books_exceeded', HttpStatus.BAD_REQUEST);
  }
}
