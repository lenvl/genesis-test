import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientModule } from './modules/client/client.module';
import { Connection } from 'typeorm';
import { ClientEntity } from './modules/client/entities/client.entity';
import { AuthorEntity } from './modules/author/entities/author-entity.dto';
import { AuthorModule } from './modules/author/author.module';
import { BookEntity } from './modules/book/entities/book.entity';
import { LockedBook } from './modules/lockedbook/entities/locked-book.entity';
import { BookModule } from './modules/book/book.module';
import { LockedBookModule } from './modules/lockedbook/locked-book.module';
import { AdminModule } from './modules/admin/admin.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST ? process.env.DB_HOST : '8.9.31.216',
      port: 3306,
      username: process.env.DB_USER ? process.env.DB_USER : 'root',
      password: process.env.DB_PASSWORD ? process.env.DB_PASSWORD : 'vfhbyjxrf123',
      database: process.env.DB_NAME ? process.env.DB_NAME : 'test_db',
      charset: 'UTF8_GENERAL_CI',
      entities: [
        ClientEntity,
        AuthorEntity,
        ClientEntity,
        BookEntity,
        LockedBook,
      ],
    }),
    AdminModule,
    AuthorModule,
    ClientModule,
    BookModule,
    LockedBookModule,
  ],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
